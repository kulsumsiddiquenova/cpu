<?php // validating if user logged in or not


require_once("auth.php");
// validating if user logged in or not
require_once("connection.php");

$user_name = $_SESSION['USERNAME'];
$email =  $_SESSION['USEREMAIL'];

$sql = "SELECT * From instructor_list WHERE email = '$email'";
$result = $conn->query($sql);

// var_dump($email);
      
while($row = $result->fetch_assoc()) {

  $f_name = $row["f_name"];
  $email = $row["email"];
  $billing_email = $row["billing_email"];
  $company_name = $row["company_name"];
  $website = $row["website"];
  $telephone = $row["telephone"];
  $user_id = $row["id"];
  $address = $row["address"];
  $city = $row["city"];
  $state = $row["state"];
  $postcode = $row["postcode"];
  $country = $row["country"];


}

$address_break = implode(".",$address);
$address_line_1 = $address_break[0];
$address_line_2 = $address_break[1];
$address_line_3 = $address_break[2];

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Account | Client | Clipping Path Universe</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #1 for full width layout with mega menu" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout/css/themes/blue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-full-width">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <?php include('header.php'); ?>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
               
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                   
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        
                        <?php
                        $notify = "";
                        if(isset($_GET['qr'])){ 
                            $notify = $_GET['qr']; }
                        
                        
                        
                       if ($notify == '5'){
                            echo "
                            <div class='panel-body'>
                            <div class='alert alert-success alert-block fade in'>
                                    <button data-dismiss='alert' class='close close-sm' type='button'>
                                        <i class='fa fa-times'></i>
                                    </button>                         
                                    <p>Record Updated Contact Details Successfully</p>
                            </div>
                            </div>";
                        }elseif ($notify == '6'){
                            echo "
                            <div class='panel-body'>
                            <div class='alert alert-success alert-block fade in'>
                                    <button data-dismiss='alert' class='close close-sm' type='button'>
                                        <i class='fa fa-times'></i>
                                    </button>                         
                                    <p>Password Updated!</p>
                            </div>
                            </div>";
                        }elseif ($notify == '11'){
                            echo "
                            <div class='panel-body'>
                            <div class='alert alert-info alert-block fade in'>
                                    <button data-dismiss='alert' class='close close-sm' type='button'>
                                        <i class='fa fa-times'></i>
                                    </button>                         
                                    <p>Record deleted successfully.</p>
                            </div>
                            </div>";
                        }
                    
                    ?>
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> <i class="fa fa-user"></i> Account Information </h1>
                        <hr>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        
                        <div class="row row-padded-rl">
                            <div class="col-md-6">
                                    <div class="portlet box green ">
                                            <div class="portlet-title">
                                                <div class="caption">Contact Details</div>
                                                <div class="tools">
                                                    <a href="" class="collapse" data-original-title="" title=""> </a>
                                                    
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <form class="form-horizontal" action="registeration_insert.php" role="form" method="get">
                                                    <div class="form-body">
                                                        
                                                        <div class="form-group">
                                                            <label class="col-md-4 control-label">Contact Name*</label>
                                                            <div class="col-md-8">
                                                                <input type="text" class="form-control input-sm" name="f_name" value="<?=$f_name?>"> </div>
                                                        </div>
                                                        <div class="form-group">
                                                                <label class="col-md-4 control-label">Email Address</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control input-sm" name="email" value="<?=$email?>" > </div>
                                                        </div>
                                                        <div class="form-group">
                                                                <label class="col-md-4 control-label">Billing Email Address</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control input-sm" name="billing_email" value="<?=$billing_email?>" > </div>
                                                                 
                                                        </div>
                                                        <div class="form-group">
                                                                <label class="col-md-4 control-label">Company Name</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control input-sm" name="company_name" value="<?=$company_name?>" > </div>
                                                        </div>
                                                        <div class="form-group">
                                                                <label class="col-md-4 control-label">Website Address</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control input-sm" name="website" value="<?=$website?>"> </div>
                                                        </div>
                                                        <div class="form-group">
                                                                <label class="col-md-4 control-label">Telephone</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control input-sm" name="telephone" value="<?=$telephone?>" > </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions right">
                                                        <input type="hidden" name="reqtype" value="update_contact_info">
                                                        <input type="hidden" name="get_user_id" value="<?php echo $user_id; ?>"/>
                                                        <button type="submit" class="btn btn38 green">Update</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                
                            </div>
                            <div class="col-md-6">
                                    <div class="portlet box green ">
                                            <div class="portlet-title">
                                                <div class="caption">Billing Address</div>
                                                <div class="tools">
                                                    <a href="" class="collapse" data-original-title="" title=""> </a>
                                                    
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <form class="form-horizontal" action="registeration_insert.php" role="form" method="get">
                                                    <div class="form-body">
                                                        
                                                        <div class="form-group">
                                                            <label class="col-md-4 control-label">Address Line 1</label>
                                                            <div class="col-md-8">
                                                                <input type="text" class="form-control input-sm" name="address_line_1" value="<?=$address_line_1?>"> </div>
                                                        </div>
                                                        <div class="form-group">
                                                                <label class="col-md-4 control-label">Address Line 2</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control input-sm" name="address_line_2" value="<?=$address_line_2?>" > </div>
                                                        </div>
                                                        <div class="form-group">
                                                                <label class="col-md-4 control-label">Address Line 3</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control input-sm" name="address_line_3" value="<?=$address_line_3?>"> </div>
                                                        </div>
                                                        <div class="form-group">
                                                                <label class="col-md-4 control-label">City/Town</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control input-sm" name="city" value="<?=$city?>"> </div>
                                                        </div>
                                                        <div class="form-group">
                                                                <label class="col-md-4 control-label">State/Town</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control input-sm" name="state" value="<?=$state?>"> </div>
                                                        </div>
                                                        <div class="form-group">
                                                                <label class="col-md-4 control-label">Postcode</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control input-sm" name="postcode" value="<?=$postcode?>"> </div>
                                                        </div>
                                                        <div class="form-group">
                                                                <label class="col-md-4 control-label">Country</label>
                                                                <div class="col-md-8">
                                                                    <select class="form-control input-sm" id="Country" name="country">
                                                                        <option value="AF">Afghanistan</option>
                                                                        <option value="AX">Aland Islands</option>
                                                                        <option value="AL">Albania</option>
                                                                        <option value="DZ">Algeria</option>
                                                                        <option value="AS">American Samoa</option>
                                                                        <option value="AD">Andorra</option>
                                                                        <option value="AO">Angola</option>
                                                                        <option value="AI">Anguilla</option>
                                                                        <option value="AQ">Antarctica</option>
                                                                        <option value="AG">Antigua And Barbuda</option>
                                                                        <option value="AR">Argentina</option>
                                                                        <option value="AM">Armenia</option>
                                                                        <option value="AW">Aruba</option>
                                                                        <option value="AU">Australia</option>
                                                                        <option value="AT">Austria</option>
                                                                        <option value="AZ">Azerbaijan</option>
                                                                        <option value="BS">Bahamas</option>
                                                                        <option value="BH">Bahrain</option>
                                                                        <option value="BD">Bangladesh</option>
                                                                        <option value="BB">Barbados</option>
                                                                        <option value="BY">Belarus</option>
                                                                        <option value="BE">Belgium</option>
                                                                        <option value="BZ">Belize</option>
                                                                        <option value="BJ">Benin</option>
                                                                        <option value="BM">Bermuda</option>
                                                                        <option value="BT">Bhutan</option>
                                                                        <option value="BO">Bolivia</option>
                                                                        <option value="BA">Bosnia And Herzegovina</option>
                                                                        <option value="BW">Botswana</option>
                                                                        <option value="BV">Bouvet Island</option>
                                                                        <option value="BR" selected="selected">Brazil</option>
                                                                        <option value="IO">British Indian Ocean Territory</option>
                                                                        <option value="BN">Brunei Darussalam</option>
                                                                        <option value="BG">Bulgaria</option>
                                                                        <option value="BF">Burkina Faso</option>
                                                                        <option value="BI">Burundi</option>
                                                                        <option value="KH">Cambodia</option>
                                                                        <option value="CM">Cameroon</option>
                                                                        <option value="CA">Canada</option>
                                                                        <option value="CV">Cape Verde</option>
                                                                        <option value="KY">Cayman Islands</option>
                                                                        <option value="CF">Central African Republic</option>
                                                                        <option value="TD">Chad</option>
                                                                        <option value="CL">Chile</option>
                                                                        <option value="CN">China</option>
                                                                        <option value="CX">Christmas Island</option>
                                                                        <option value="CC">Cocos (keeling) Islands</option>
                                                                        <option value="CO">Colombia</option>
                                                                        <option value="KM">Comoros</option>
                                                                        <option value="CG">Congo</option>
                                                                        <option value="CD">Congo, Democratic Republic</option>
                                                                        <option value="CK">Cook Islands</option>
                                                                        <option value="CR">Costa Rica</option>
                                                                        <option value="CI">Cote D'ivoire</option>
                                                                        <option value="HR">Croatia</option>
                                                                        <option value="CU">Cuba</option>
                                                                        <option value="CY">Cyprus</option>
                                                                        <option value="CZ">Czech Republic</option>
                                                                        <option value="DK">Denmark</option>
                                                                        <option value="DJ">Djibouti</option>
                                                                        <option value="DM">Dominica</option>
                                                                        <option value="DO">Dominican Republic</option>
                                                                        <option value="EC">Ecuador</option>
                                                                        <option value="EG">Egypt</option>
                                                                        <option value="SV">El Salvador</option>
                                                                        <option value="GQ">Equatorial Guinea</option>
                                                                        <option value="ER">Eritrea</option>
                                                                        <option value="EE">Estonia</option>
                                                                        <option value="ET">Ethiopia</option>
                                                                        <option value="FK">Falkland Islands (malvinas)</option>
                                                                        <option value="FO">Faroe Islands</option>
                                                                        <option value="FJ">Fiji</option>
                                                                        <option value="FI">Finland</option>
                                                                        <option value="FR">France</option>
                                                                        <option value="GF">French Guiana</option>
                                                                        <option value="PF">French Polynesia</option>
                                                                        <option value="TF">French Southern Territories</option>
                                                                        <option value="GA">Gabon</option>
                                                                        <option value="GM">Gambia</option>
                                                                        <option value="GE">Georgia</option>
                                                                        <option value="DE">Germany</option>
                                                                        <option value="GH">Ghana</option>
                                                                        <option value="GI">Gibraltar</option>
                                                                        <option value="GR">Greece</option>
                                                                        <option value="GL">Greenland</option>
                                                                        <option value="GD">Grenada</option>
                                                                        <option value="GP">Guadeloupe</option>
                                                                        <option value="GU">Guam</option>
                                                                        <option value="GT">Guatemala</option>
                                                                        <option value="GN">Guinea</option>
                                                                        <option value="GW">Guinea-bissau</option>
                                                                        <option value="GY">Guyana</option>
                                                                        <option value="HT">Haiti</option>
                                                                        <option value="HM">Heard Island/mcdonald Islands</option>
                                                                        <option value="VA">Holy See (vatican City State)</option>
                                                                        <option value="HN">Honduras</option>
                                                                        <option value="HK">Hong Kong</option>
                                                                        <option value="HU">Hungary</option>
                                                                        <option value="IS">Iceland</option>
                                                                        <option value="IN">India</option>
                                                                        <option value="ID">Indonesia</option>
                                                                        <option value="IR">Iran</option>
                                                                        <option value="IQ">Iraq</option>
                                                                        <option value="IE">Ireland</option>
                                                                        <option value="IL">Israel</option>
                                                                        <option value="IT">Italy</option>
                                                                        <option value="JM">Jamaica</option>
                                                                        <option value="JP">Japan</option>
                                                                        <option value="JO">Jordan</option>
                                                                        <option value="KZ">Kazakhstan</option>
                                                                        <option value="KE">Kenya</option>
                                                                        <option value="KI">Kiribati</option>
                                                                        <option value="KP">Korea, Democratic Republic</option>
                                                                        <option value="KR">Korea, Republic Of</option>
                                                                        <option value="KW">Kuwait</option>
                                                                        <option value="KG">Kyrgyzstan</option>
                                                                        <option value="LA">Lao Democratic Republic</option>
                                                                        <option value="LV">Latvia</option>
                                                                        <option value="LB">Lebanon</option>
                                                                        <option value="LS">Lesotho</option>
                                                                        <option value="LR">Liberia</option>
                                                                        <option value="LY">Libyan Arab Jamahiriya</option>
                                                                        <option value="LI">Liechtenstein</option>
                                                                        <option value="LT">Lithuania</option>
                                                                        <option value="LU">Luxembourg</option>
                                                                        <option value="MO">Macao</option>
                                                                        <option value="MK">Macedonia</option>
                                                                        <option value="MG">Madagascar</option>
                                                                        <option value="MW">Malawi</option>
                                                                        <option value="MY">Malaysia</option>
                                                                        <option value="MV">Maldives</option>
                                                                        <option value="ML">Mali</option>
                                                                        <option value="MT">Malta</option>
                                                                        <option value="MH">Marshall Islands</option>
                                                                        <option value="MQ">Martinique</option>
                                                                        <option value="MR">Mauritania</option>
                                                                        <option value="MU">Mauritius</option>
                                                                        <option value="YT">Mayotte</option>
                                                                        <option value="MX">Mexico</option>
                                                                        <option value="FM">Micronesia</option>
                                                                        <option value="MD">Moldova</option>
                                                                        <option value="MC">Monaco</option>
                                                                        <option value="MN">Mongolia</option>
                                                                        <option value="MS">Montserrat</option>
                                                                        <option value="MA">Morocco</option>
                                                                        <option value="MZ">Mozambique</option>
                                                                        <option value="MM">Myanmar</option>
                                                                        <option value="NA">Namibia</option>
                                                                        <option value="NR">Nauru</option>
                                                                        <option value="NP">Nepal</option>
                                                                        <option value="NL">Netherlands</option>
                                                                        <option value="AN">Netherlands Antilles</option>
                                                                        <option value="NC">New Caledonia</option>
                                                                        <option value="NZ">New Zealand</option>
                                                                        <option value="NI">Nicaragua</option>
                                                                        <option value="NE">Niger</option>
                                                                        <option value="NG">Nigeria</option>
                                                                        <option value="NU">Niue</option>
                                                                        <option value="NF">Norfolk Island</option>
                                                                        <option value="MP">Northern Mariana Islands</option>
                                                                        <option value="NO">Norway</option>
                                                                        <option value="OM">Oman</option>
                                                                        <option value="PK">Pakistan</option>
                                                                        <option value="PW">Palau</option>
                                                                        <option value="PS">Palestinian Territory</option>
                                                                        <option value="PA">Panama</option>
                                                                        <option value="PG">Papua New Guinea</option>
                                                                        <option value="PY">Paraguay</option>
                                                                        <option value="PE">Peru</option>
                                                                        <option value="PH">Philippines</option>
                                                                        <option value="PN">Pitcairn</option>
                                                                        <option value="PL">Poland</option>
                                                                        <option value="PT">Portugal</option>
                                                                        <option value="PR">Puerto Rico</option>
                                                                        <option value="QA">Qatar</option>
                                                                        <option value="RE">Reunion</option>
                                                                        <option value="RO">Romania</option>
                                                                        <option value="RU">Russian Federation</option>
                                                                        <option value="RW">Rwanda</option>
                                                                        <option value="SH">Saint Helena</option>
                                                                        <option value="KN">Saint Kitts And Nevis</option>
                                                                        <option value="LC">Saint Lucia</option>
                                                                        <option value="PM">Saint Pierre And Miquelon</option>
                                                                        <option value="VC">St Vincent &amp; The Grenadines</option>
                                                                        <option value="WS">Samoa</option>
                                                                        <option value="SM">San Marino</option>
                                                                        <option value="ST">Sao Tome And Principe</option>
                                                                        <option value="SA">Saudi Arabia</option>
                                                                        <option value="SN">Senegal</option>
                                                                        <option value="CS">Serbia And Montenegro</option>
                                                                        <option value="SC">Seychelles</option>
                                                                        <option value="SL">Sierra Leone</option>
                                                                        <option value="SG">Singapore</option>
                                                                        <option value="SK">Slovakia</option>
                                                                        <option value="SI">Slovenia</option>
                                                                        <option value="SB">Solomon Islands</option>
                                                                        <option value="SO">Somalia</option>
                                                                        <option value="ZA">South Africa</option>
                                                                        <option value="GS">South Georgia/sandwich Isles</option>
                                                                        <option value="ES">Spain</option>
                                                                        <option value="LK">Sri Lanka</option>
                                                                        <option value="SD">Sudan</option>
                                                                        <option value="SR">Suriname</option>
                                                                        <option value="SJ">Svalbard And Jan Mayen</option>
                                                                        <option value="SZ">Swaziland</option>
                                                                        <option value="SE">Sweden</option>
                                                                        <option value="CH">Switzerland</option>
                                                                        <option value="SY">Syrian Arab Republic</option>
                                                                        <option value="TW">Taiwan</option>
                                                                        <option value="TJ">Tajikistan</option>
                                                                        <option value="TZ">Tanzania</option>
                                                                        <option value="TH">Thailand</option>
                                                                        <option value="TL">Timor-leste</option>
                                                                        <option value="TG">Togo</option>
                                                                        <option value="TK">Tokelau</option>
                                                                        <option value="TO">Tonga</option>
                                                                        <option value="TT">Trinidad And Tobago</option>
                                                                        <option value="TN">Tunisia</option>
                                                                        <option value="TR">Turkey</option>
                                                                        <option value="TM">Turkmenistan</option>
                                                                        <option value="TC">Turks And Caicos Islands</option>
                                                                        <option value="TV">Tuvalu</option>
                                                                        <option value="UG">Uganda</option>
                                                                        <option value="UA">Ukraine</option>
                                                                        <option value="AE">United Arab Emirates</option>
                                                                        <option value="GB">United Kingdom</option>
                                                                        <option value="US">United States</option>
                                                                        <option value="UM">Us Minor Outlying Islands</option>
                                                                        <option value="UY">Uruguay</option>
                                                                        <option value="UZ">Uzbekistan</option>
                                                                        <option value="VU">Vanuatu</option>
                                                                        <option value="VE">Venezuela</option>
                                                                        <option value="VN">Viet Nam</option>
                                                                        <option value="VG">Virgin Islands, British</option>
                                                                        <option value="VI">Virgin Islands, U.s.</option>
                                                                        <option value="WF">Wallis And Futuna</option>
                                                                        <option value="EH">Western Sahara</option>
                                                                        <option value="YE">Yemen</option>
                                                                        <option value="ZM">Zambia</option>
                                                                        <option value="ZW">Zimbabwe</option>
                                                                        <option value="EU">European Union</option>
                                                                        <option value="WW">Worldwide</option>
                                                                
                                                                    </select>
                                                                </div>
                                                            </div>

                                                    </div>
                                                    <div class="form-actions right">
                                                        <input type="hidden" name="reqtype" value="update_billing_address">
                                                        <input type="hidden" name="get_user_id" value="<?php echo $user_id; ?>"/>
                                                        <button type="submit" class="btn btn38 green">Update</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                
                            </div>
                        
                        </div>
                        <div class="row row-padded-rl">
                                <div class="col-md-6">
                                        <div class="portlet box green ">
                                                <div class="portlet-title">
                                                    <div class="caption">Change Password</div>
                                                    <div class="tools">
                                                        <a href="" class="collapse" data-original-title="" title=""> </a>
                                                        
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <form class="form-horizontal" action="registeration_insert.php" role="form" method="get">
                                                        <div class="form-body">
                                                            
                                                            <div class="form-group">
                                                                <label class="col-md-4 control-label">Current Password</label>
                                                                <div class="col-md-8">
                                                                    <input type="password" class="form-control input-sm"  name="old_password"> </div>
                                                            </div>
                                                            <div class="form-group">
                                                                    <label class="col-md-4 control-label">New Password</label>
                                                                    <div class="col-md-8">
                                                                        <input type="password" class="form-control input-sm" name="new_password"> </div>
                                                            </div>
                                                            
                                                        </div>
                                                        <div class="form-actions right">
                                                            <input type="hidden" name="reqtype" value="update_password">
                                                            <input type="hidden" name="get_user_id" value="<?php echo $user_id; ?>"/>
                                                            <button type="submit" class="btn btn38 green">Update</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                    
                                </div>
                                <div class="col-md-6">
                                        <div class="portlet box green ">
                                                <div class="portlet-title">
                                                    <div class="caption">FTP Login</div>
                                                    <div class="tools">
                                                        <a href="" class="collapse" data-original-title="" title=""> </a>
                                                        
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <form class="form-horizontal" role="form">
                                                        <div class="form-body">
                                                            
                                                            <div class="form-group">
                                                                <label class="col-md-4 control-label"><b>FTP Host</b></label>
                                                                <div class="col-md-8">
                                                                        <p class="form-control-static">ftp.clippingpathuniverse.com </p></div>
                                                            </div>
                                                            <div class="form-group">
                                                                    <label class="col-md-4 control-label"><b>Username</b></label>
                                                                    <div class="col-md-8">
                                                                            <p class="form-control-static">mark@clippingpathuniverse.com </p></div>
                                                            </div>
                                                            <div class="form-group">
                                                                    <label class="col-md-4 control-label"><b>Password</b></label>
                                                                    <div class="col-md-8">
                                                                            <p class="form-control-static">Jm7#4FVB^j%3(@M04$ </p></div>
                                                            </div>
                                                            
    
                                                        </div>
                                                        
                                                    </form>
                                                </div>
                                            </div>
                                    
                                </div>
                            
                            </div>
                        
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
          
            <?php include('footer.php'); ?>
            <!-- END FOOTER -->
        </div>
        
        <!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<script src="assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="assets/pages/scripts/form-samples.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>