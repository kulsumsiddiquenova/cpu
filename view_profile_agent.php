<?php 

// validating if user logged in or not
//require_once("auth.php");
// validating if user logged in or not
    
    session_start();
     $login_id = $_SESSION['USERNAME'];
    require_once("connection.php");

       

      $sql = "SELECT * From instructor_list WHERE login_id = '$login_id'";
      $result = $conn->query($sql);
   
            
     while($row = $result->fetch_assoc()) {
        
        $type = $row["type"];
        $f_name = $row["f_name"];
        $l_name = $row["l_name"];
        $login_id = $row["login_id"];
        $password = $row["password"];
        $user_id = $row["id"];

     }
?>


<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <!-- BEGIN Authentication and DB Connection kulsum01s -->
    
    <!-- end Authentication and DB Connection kulsum01e -->
    <head>
        <meta charset="utf-8" />
        <title>IPSTQM | Edit User</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS kulsum02start-->
         <link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS kulsum02end-->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <?php include "header.php";?>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                <?php include "sidebar_menu_agent.php";?>
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                            
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> Edit System User
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                   
                     <?php
                        $notify = "";
                        if(isset($_GET['qr'])){ 
                            $notify = $_GET['qr']; }
                        
                        
                        
                       if ($notify == '5'){
                            echo "
                            <div class='panel-body'>
                            <div class='alert alert-success alert-block fade in'>
                                    <button data-dismiss='alert' class='close close-sm' type='button'>
                                        <i class='fa fa-times'></i>
                                    </button>                         
                                    <p>Record has been added successfully!</p>
                            </div>
                            </div>";
                        }elseif ($notify == '6'){
                            echo "
                            <div class='panel-body'>
                            <div class='alert alert-success alert-block fade in'>
                                    <button data-dismiss='alert' class='close close-sm' type='button'>
                                        <i class='fa fa-times'></i>
                                    </button>                         
                                    <p>Record has been Updated successfully!</p>
                            </div>
                            </div>";
                        }elseif ($notify == '11'){
                            echo "
                            <div class='panel-body'>
                            <div class='alert alert-info alert-block fade in'>
                                    <button data-dismiss='alert' class='close close-sm' type='button'>
                                        <i class='fa fa-times'></i>
                                    </button>                         
                                    <p>Record deleted successfully.</p>
                            </div>
                            </div>";
                        }
                    
                    ?>
                    
                    
                        
                    <!--notification end-->
                        <!-- BEGIN Kaizen page Content kulsum03start-->
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-user font-red-sunglo"></i>
                                                        <span class="caption-subject font-red-sunglo bold uppercase">System User</span>
                                                        <span class="caption-helper">Edit / Delete user from this system</span>
                                                    </div>
                                                    
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="agent_edit_profile.php" method="POST" class="form-horizontal">
                                                        <div class="form-body">
                                                        
                                                            <div class="form-group">
                                                            <label class="col-md-3 control-label">Type</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" name="type" value="<?php echo $type;?>"  disabled>
                                                                </div>
                                                             </div>
                                                             
                                                            <div class="form-group">
                                                            <label class="col-md-3 control-label">First Name</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" name="f_name" value="<?php echo $f_name;?>"  disabled>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                            <label class="col-md-3 control-label">Last Name</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" name="l_name" value="<?php echo $l_name;?>" disabled>
                                                                </div>
                                                            </div>
                                                           
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">User ID</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">
                                                                            <i class="fa fa-user"></i>
                                                                        </span>
                                                                        <input type="text" class="form-control" name="login_id"  value="<?php echo $login_id;?>" disabled> </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">Password</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">
                                                                            <i class="fa fa-lock"></i>
                                                                        </span>
                                                                        <input type="password" class="form-control" name="password"  value="<?php echo $password;?>" placeholder=" Password"> </div>
                                                                </div>
                                                            </div>
                                                           
                                                            
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                             <input type="hidden" name="reqtype" value="edit">
                                                              <input type="hidden" name="get_user_id" value="<?php echo $user_id; ?>"/>
                                                               <button type="submit" class="btn blue">
                                                                <i class="fa fa-check"></i> Save</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                     
                                                    <!-- END FORM-->
                                                </div>
                                            </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                                
                            </div>
                        </div>
                        
             
                        <!-- END Kaizen page Content kulsum03end-->                 
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->

            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <?php include "footer.php"; ?>
            <!-- END FOOTER -->
        </div>
        <!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS kulsum04start-->
        <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS kulsum04end-->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS kulsum05start -->
        <script src="assets/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS kulsum05end -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>