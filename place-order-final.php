<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<!-- BEGIN Authentication and DB Connection kulsum01s -->
<?php // validating if user logged in or not
// validating if user logged in or not
require_once("connection.php");
?>
<!-- end Authentication and DB Connection kulsum01e -->
<head>
    <meta charset="utf-8" />
    <title>Place Order | Clipping Path Universe</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
     
    <!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGINS kulsum02start-->
    <link href="assets/global/plugins/dropzone/dropzone.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/dropzone/basic.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS kulsum02end-->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" /> 
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <div class="page-wrapper">
        <!-- BEGIN HEADER -->
        <?php include "header.php";?>
		<!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <?php include "sidebar_menu.php";?>
			<!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                        
                    <!-- BEGIN PAGE TITLE-->
                    <h1 class="page-title"> Place Orders</h1>
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
                        <?php
                            $notify = "";
                            if(isset($_GET['qr'])){ 
                                $notify = $_GET['qr']; }
                            
                            
                            
                           if ($notify == '5'){
                                echo "
                                <div class='panel-body'>
                                <div class='alert alert-success alert-block fade in'>
                                        <button data-dismiss='alert' class='close close-sm' type='button'>
                                            <i class='fa fa-times'></i>
                                        </button>                         
                                        <p>Record has been added successfully!</p>
                                </div>
                                </div>";
                            }elseif ($notify == '6'){
                                echo "
                                <div class='panel-body'>
                                <div class='alert alert-success alert-block fade in'>
                                        <button data-dismiss='alert' class='close close-sm' type='button'>
                                            <i class='fa fa-times'></i>
                                        </button>                         
                                        <p>Record has been Updated successfully!</p>
                                </div>
                                </div>";
                            }elseif ($notify == '11'){
                                echo "
                                <div class='panel-body'>
                                <div class='alert alert-info alert-block fade in'>
                                        <button data-dismiss='alert' class='close close-sm' type='button'>
                                            <i class='fa fa-times'></i>
                                        </button>                         
                                        <p>Record deleted successfully.</p>
                                </div>
                                </div>";
                            }
                        
                        ?>
 
                <!--notification end-->

			<!-- BEGIN Kaizen page Content kulsum03start-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-user font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">Orders</span>
                                            <span class="caption-helper">Place your orders to this system</span>
                                        </div>
                                        
                                    </div>
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                    <form action="place-order-insert.php" method="POST" enctype="multipart/form-data" class="form-horizontal dropzone dropzone-file-area"  name="file_name" id="my-dropzone"  style="width: 900px; margin-right: 0px; margin-left: 0px;">
                                        <div class="form-body" >
                                            <div class="col-md-6 col-md-offset-3" >
												   <h3 class="sbold">Drop files here or click to upload</h3>
													<p> This is just a demo dropzone. Selected files are not actually uploaded. </p>
                                                     <input type="hidden" name="reqtype" value="add">
                                                     <input type="hidden" name="service" value="<?php echo "$_GET[service]"; ?>">
                                                     <input type="hidden" name="file_format" value="<?php echo "$_GET[file_format]"; ?>">
                                                     <input type="hidden" name="quantity" value="<?php echo "$_GET[quantity]"; ?>">
                                                     <input type="hidden" name="turnaround" value="<?php echo "$_GET[turnaround]"; ?>">
                                                     <input type="hidden" name="quotation" value="<?php echo "$_GET[quotation]"; ?>">
                                                     <input type="hidden" name="instruction" value="<?php echo "$_GET[instruction]"; ?>">
                                                </div>
                                            </div>
									
                                          
                                        </form>
                                           <div class="col-md-6 col-md-offset-5">
                                               <button type="submit" class="btn green" >Submit</button> 
                                               <button onclick="window.open('dashboard.php','_top')" type="button" class="btn default">Cancel</button> 
                                           </div>
                                           
										
                                        <!-- END FORM-->
                                    </div>
                                </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                    
                </div>
            </div>
                    
          
            </div>
			<!-- END Kaizen page Content kulsum03end-->					
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "footer.php"; ?>
<!-- END FOOTER -->
</div>
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->


<!-- BEGIN CORE PLUGINS -->
     <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS kulsum04start-->
	 <script src="assets/global/plugins/dropzone/dropzone.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS kulsum04end-->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS kulsum05start -->
    <script src="assets/pages/scripts/form-dropzone.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS kulsum05end -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
    <script src="assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
    <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
    <!-- END THEME LAYOUT SCRIPTS -->
</body>
</html>	