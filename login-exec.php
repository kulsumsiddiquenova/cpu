<?php
 //Start session
 session_start();
 
 //Include database connection details
 require_once('config.php');
 
 //Array to store validation errors
 $errmsg_arr = array();
 
 //Validation error flag
 $errflag = false;
 
 //Connect to mysql server
 $link = mysqli_connect (DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
 if(!$link) {
  die('Failed to connect to server: ' . mysql_error());
 }
 
 
 
 //Function to sanitize values received from the form. Prevents SQL injection
 function cleanInput($input) {
 
  $search = array(
    '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
    '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
    '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
    '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
  );
 
    $output = preg_replace($search, '', $input);
    return $output;
  }
function clean($input) {
    if (is_array($input)) {
        foreach($input as $var=>$val) {
            $output[$var] = sanitize($val);
        }
    }
    else {
        if (get_magic_quotes_gpc()) {
            $input = stripslashes($input);
        }
        $input  = cleanInput($input);
        $output = strip_tags($input);
    }
    return $output;
}
 
 //Sanitize the POST values
 $login = clean($_POST['username']);
 $password = clean($_POST['password']);
// $password = strip_tags(mysql_real_escape_string(trim($_POST['password'])));
//  password_verify('rasmuslerdorf', $hash)
 //Input Validations
//  if($login == '') {
//   $errmsg_arr[] = 'Login ID missing';
//   $errflag = true;
//  }
//  if($password == '') {  $errmsg_arr[] = 'Password missing';
//   $errflag = true;
//  }
 
 //If there are input validations, redirect back to the login form
 if($errflag) {
  $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
  session_write_close();
  header("location: lost_in_error.html");
  exit();
 }
 
 //Create query
 $qry="SELECT f_name,email, password,type FROM instructor_list WHERE email='$login'";
 $result=mysqli_query($link, $qry);
 
 //Check whether the query was successful or not
 if($result) {
    if(mysqli_num_rows($result)) {
        //Login Successful
        session_regenerate_id();
        $member = mysqli_fetch_assoc($result);
        $type = $member['type'];
        $password_hash = $member['password'];
        $_SESSION['USERNAME'] = $member['f_name'];
        $_SESSION['USEREMAIL'] = $login;
        $login_id = $_SESSION['USERNAME'];
         
        session_write_close(); 
        if(password_verify($password,$password_hash)){
            // var_dump($password);
            // var_dump($password_hash);
            // var_dump(password_verify($password,$password_hash));
            if($type =="Client"){
                header("location: client_dashboard.php?q=$login_id");
            }elseif(  $type =="Admin" ){
                header("location: admin_dashboard.php?q=$login_id");
            }elseif(  $type =="Production" ){
                header("location: production_dashboard.php?q=$login_id");
            }
        }else {
            //Login failed
            header("location: wrong_login.html");
            exit();
            }
        }
    }else {
        die("Query failed");
    }
?>