<?php
	//Start session
	session_start();
	
	//Unset the variables stored in session
	unset($_SESSION['USERNAME']);

	header("location: index.php");
	exit();
	
	?>
