<?php

require_once("connection.php");

    $notify = "";
    echo $notify;
    if (isset($_POST['reqtype'])){ 
      $notify = $_POST['reqtype'];  
    }elseif (isset($_GET['reqtype'])){
      $notify = $_GET['reqtype'];
    }


  
    if($notify == 'register'){


    $f_name = trim($_POST["fullname"]);
  	$email = trim($_POST["email"]);
    $password =  password_hash(trim($_POST["password"]),PASSWORD_DEFAULT);
   

	$insert_order = "INSERT INTO instructor_list 
					(f_name,email,type,password)
					 VALUES
           ('$f_name','$email','Client','$password');";
           
    if (!file_exists("upload/". $email)) {
            mkdir("upload/". $email);
    }       

    if( ($conn->query($insert_order) === TRUE) ) {
        // echo "inserted case";
         header("location: index.php?qr=5");
             exit();
    }

   }elseif($notify == 'del'){
     $oldvalue = trim($_GET['oldvalue']);
  
      $qry = "DELETE FROM instructor_list WHERE id='$oldvalue'";
   
       if($conn->query($qry) === TRUE) {
          header("location: user.php?qr=11");
        exit();
       }
   }elseif($notify == 'update_contact_info'){
        
        $user_id = trim($_GET["get_user_id"]);
        $f_name = trim($_GET["f_name"]);
        $email = trim($_GET["email"]);
        $billing_email = trim($_GET["billing_email"]);
        $company_name = trim($_GET["company_name"]);
        $website = trim($_GET["website"]); 
        $telephone = trim($_GET["telephone"]);
      
      $update_user = "UPDATE instructor_list SET f_name='$f_name', email='$email', billing_email='$billing_email', company_name='$company_name', telephone='$telephone',
                      website='$website' WHERE id=$user_id";
      //  var_dump($update_user);
        
       if($conn->query($update_user) === TRUE) {
          header("location: account.php?qr=5");
        exit();
       }
   }elseif($notify == 'update_password'){
        
        $user_id = trim($_GET["get_user_id"]);
        $old_password = trim($_GET["old_password"]);
        $new_password = trim($_GET["new_password"]);

        $sql = "SELECT id,password From instructor_list WHERE id =$user_id";
        $result = $conn->query($sql);

       while($row = $result->fetch_assoc()) {
            $password_hash = $row["password"];
        }

        if(password_verify($old_password,$password_hash)){
          $password =  password_hash($new_password,PASSWORD_DEFAULT);
        }
    
      $update_user = "UPDATE instructor_list SET password='$password' WHERE id=$user_id";

      if($conn->query($update_user) === TRUE) {
          header("location: account.php?qr=6");
        exit();
      }
  }elseif($notify == 'update_billing_address'){
        
      
      $user_id = trim($_GET["get_user_id"]);
      $address_line_1 = trim($_GET["address_line_1"]);
      $address_line_2 = trim($_GET["address_line_2"]);
      $address_line_3 = trim($_GET["address_line_3"]);
      $city = trim($_GET["city"]);
      $state = trim($_GET["state"]); 
      $postcode = trim($_GET["postcode"]);
      $country = trim($_GET["country"]);

      $address = $address_line_1.".". $address_line_2.".".$address_line_3;
    

    $update_user = "UPDATE instructor_list SET address='$address', city ='$city', state ='$state', postcode ='$postcode', country='$country'  WHERE id=$user_id";

    if($conn->query($update_user) === TRUE) {
        header("location: account.php?qr=6");
      exit();
    }
  }

?>