<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <!-- BEGIN Authentication and DB Connection kulsum01s -->
    <?php // validating if user logged in or not
    // validating if user logged in or not
    require_once("connection.php");
    ?>
    <!-- end Authentication and DB Connection kulsum01e -->
    <head>
        <meta charset="utf-8" />
        <title>DASIA | View Orders</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS kulsum02start-->
         <link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS kulsum02end-->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <?php include "header.php";?>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                <?php include "sidebar_menu.php";?>
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                            
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> View Orders
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                    <?php
                        $notify = "";
                        if(isset($_GET['qr'])){ 
                            $notify = $_GET['qr']; }
                        
                        
                        
                       if ($notify == '5'){
                            echo "
                            <div class='panel-body'>
                            <div class='alert alert-success alert-block fade in'>
                                    <button data-dismiss='alert' class='close close-sm' type='button'>
                                        <i class='fa fa-times'></i>
                                    </button>                         
                                    <p>Record has been added successfully!</p>
                            </div>
                            </div>";
                        }elseif ($notify == '6'){
                            echo "
                            <div class='panel-body'>
                            <div class='alert alert-success alert-block fade in'>
                                    <button data-dismiss='alert' class='close close-sm' type='button'>
                                        <i class='fa fa-times'></i>
                                    </button>                         
                                    <p>Record has been Updated successfully!</p>
                            </div>
                            </div>";
                        }elseif ($notify == '7'){
                            echo "
                            <div class='panel-body'>
                            <div class='alert alert-success alert-block fade in'>
                                    <button data-dismiss='alert' class='close close-sm' type='button'>
                                        <i class='fa fa-times'></i>
                                    </button>                         
                                    <p>Positve email response sent successfully!</p>
                            </div>
                            </div>";
                        }elseif ($notify == '8'){
                            echo "
                            <div class='panel-body'>
                            <div class='alert alert-success alert-block fade in'>
                                    <button data-dismiss='alert' class='close close-sm' type='button'>
                                        <i class='fa fa-times'></i>
                                    </button>                         
                                    <p>Negative email response sent successfully!</p>
                            </div>
                            </div>";
                        }elseif ($notify == '11'){
                            echo "
                            <div class='panel-body'>
                            <div class='alert alert-info alert-block fade in'>
                                    <button data-dismiss='alert' class='close close-sm' type='button'>
                                        <i class='fa fa-times'></i>
                                    </button>                         
                                    <p>Record deleted successfully.</p>
                            </div>
                            </div>";
                        }
                    
                    ?>
                    
                    
                        
                    <!--notification end-->
                        
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase">Orders</span>
                                        </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                                            <thead>
                                                <tr>
                                                    <th>Trip Type</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Pickup Date</th>
                                                    <th>From</th>
                                                    <th>To</th>
                                                    <th>Action</th>
                                                  
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                 <tr>
                                                    <th>Trip Type</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Pickup Date</th>
                                                    <th>From</th>
													<th>To</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                <?php
                                                
                                                $sql = "SELECT * FROM order_data";
                                                $result = $conn->query($sql);
                                                
                                                if ($result->num_rows > 0) {
                                                    // output data of each row
                                                    while($row = $result->fetch_assoc()) {
                                                        
                                                    $order_id = $row['id'];
                                                    $trip_type = $row['trip_type'];
                                                    $f_name=  $row['f_name'];
                                                    $email = $row['email'];
                                                    $pickup_date = $row['pickup_date'];
                                                    $pickup_location = $row['pickup_location'];
                                                    $drop_location = $row['drop_location'];
													$editlink=  "'reply_order.php?id=$order_id'";
                                                    echo "  <tr>
                                                                <td>$trip_type</td>
                                                                
                                                                <td>$f_name</td>

                                                                <td>$email</td>
                                                               
                                                                <td>$pickup_date</td>

                                                                <td>$pickup_location</td>

                                                                <td>$drop_location</td>

                                                                <td>
                                                                  <center>
                                                                    <button type=\"button\" class=\"btn btn-info btn-xs\" onclick=\"window.location = $editlink\">View/Reply
                                                                    </button>
                                                                  </center>
                                                                </td>
                                                                
                                                            </tr>";

                                                   
                                                    }
                                                } else {
                                                    echo "0 results";
                                                }
                                                
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                                
                            </div>
                        </div>
                        <!-- END Kaizen page Content kulsum03end-->                 
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->

            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <?php include "footer.php"; ?>
            <!-- END FOOTER -->
        </div>
        <!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS kulsum04start-->
        <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS kulsum04end-->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS kulsum05start -->
        <script src="assets/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS kulsum05end -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>