<?php // validating if user logged in or not


require_once("auth.php");
// validating if user logged in or not
require_once("connection.php");

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title>Place Order | Client | Clipping Path Universe </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #1 for buttons extension demos" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"
    />
    <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->

    <link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css"
    />
    <link href="assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"
    />
    <link href="assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/layouts/layout/css/themes/blue.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" /> </head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-full-width">
    <div class="page-wrapper">
        <!-- BEGIN HEADER -->
        <?php include('header.php'); ?>
        </div>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse in" aria-expanded="true">
                    <!-- END SIDEBAR MENU -->
                 
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->


                    <!-- BEGIN PAGE TITLE-->

                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->

                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <!-- Begin: life time stats -->
                            <div class="portlet light portlet-fit portlet-datatable bordered">
                                <div class="portlet-title portlet-color-bordr">
                                    <div class="caption">
                                        <i class="fa fa-plus-circle"></i>
                                        <span class="caption-subject sbold uppercase">PLACE AN ORDER</span>
                                    </div>
                                    <div class="actions">
                                        <button type="button" class="btn btn-circle btn-sm green-meadow">
                                            <i class="fa fa-list-ul"></i> View all orders</button>

                                    </div>
                                </div>
                                <div class="portlet-body form">

                                    <form action="place-order-insert.php" class="form-horizontal" method="get">
                                        <div class="form-body form-body-paded">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Select the service you want</label>
                                                <div class="col-md-9">
                                                    <div class="mt-checkbox-inline">
                                                        <label class="mt-checkbox mt-checkbox-outline col-md-5">
                                                            <input type="checkbox" id="inlineCheckbox21" value="Remove Background"  name="service[]">Remove Background
                                                            <i style="color: mediumblue;" class="fa fa-info-circle tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top"></i>
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-checkbox mt-checkbox-outline col-md-6">
                                                            <input type="checkbox" id="inlineCheckbox21" value="Color/Brightness Enhancement" name="service[]">Color/Brightness Enhancement
                                                            <i style="color: mediumblue;" class="fa fa-info-circle tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top"></i>
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-checkbox mt-checkbox-outline col-md-5">
                                                            <input type="checkbox" id="inlineCheckbox21" value="Clipping Path" name="service[]">Clipping Path
                                                            <i style="color: mediumblue;" class="fa fa-info-circle tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top"></i>
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-checkbox mt-checkbox-outline col-md-5">
                                                            <input type="checkbox" id="inlineCheckbox21" value="Manipulation/Neck Joint" name="service[]">Manipulation/Neck Joint
                                                            <i style="color: mediumblue;" class="fa fa-info-circle tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top"></i>
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-checkbox mt-checkbox-outline col-md-5">
                                                            <input type="checkbox" id="inlineCheckbox21" value="Drop Shadow" name="service[]">Drop Shadow
                                                            <i style="color: mediumblue;" class="fa fa-info-circle tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top"></i>
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-checkbox mt-checkbox-outline col-md-5">
                                                            <input type="checkbox" id="inlineCheckbox21" value="Resizing/Optimisation" name="service[]">Resizing/Optimisation
                                                            <i style="color: mediumblue;" class="fa fa-info-circle tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top"></i>
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-checkbox mt-checkbox-outline col-md-5">
                                                            <input type="checkbox" id="inlineCheckbox21" value="Mirror Effect" name="service[]">Mirror Effect
                                                            <i style="color: mediumblue;" class="fa fa-info-circle tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top"></i>
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-checkbox mt-checkbox-outline col-md-5">
                                                            <input type="checkbox" id="inlineCheckbox21" value="Vector Conversion" name="service[]">Vector Conversion
                                                            <i style="color: mediumblue;" class="fa fa-info-circle tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top"></i>
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-checkbox mt-checkbox-outline col-md-5">
                                                            <input type="checkbox" id="inlineCheckbox21" value="Retouching/Airbrushing" name="service[]">Retouching/Airbrushing
                                                            <i style="color: mediumblue;" class="fa fa-info-circle tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top"></i>
                                                            <span></span>
                                                        </label>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Return File Format</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-image"></i>
                                                        </span>
                                                        <select name="file_format" id="format" class="form-control" required="">

                                                            <option id="JPEG white background">JPEG white background</option>
                                                            <option id="JPEG custom background">JPEG custom background</option>
                                                            <option id="JPEG original background">JPEG original background</option>
                                                            <option id="PSD white background">PSD white background</option>
                                                            <option id="PSD transparent background">PSD transparent background</option>
                                                            <option id="PSD layer mask">PSD layer mask</option>
                                                            <option id="PSD custom background">PSD custom background</option>
                                                            <option id="PSD original background">PSD original background</option>
                                                            <option id="TIFF white background">TIFF white background</option>
                                                            <option id="TIFF transparent background">TIFF transparent background</option>
                                                            <option id="TIFF layer mask">TIFF layer mask</option>
                                                            <option id="TIFF custom background">TIFF custom background</option>
                                                            <option id="TIFF original background">TIFF original background</option>
                                                            <option id="PNG transparent background">PNG transparent background</option>
                                                            <option id="PNG white background">PNG white background</option>
                                                            <option id="PNG custom background">PNG custom background</option>
                                                            <option id="Illustrator (AI)">Illustrator (AI)</option>
                                                            <option id="Illustrator (EPS)">Illustrator (EPS)</option>
                                                            <option id="Illustrator (SVG)">Illustrator (SVG)</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Quantity</label>
                                                <div class="col-md-4">
                                                    <input id="touchspin_5" type="text"  name="quantity"> </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Deadline</label>
                                                <div class="col-md-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-clock-o"></i>
                                                        </span>
                                                        <select name="turnaround" class="form-control" required="">
                                                            <option value="" selected="selected">Desired turnaround</option>
                                                            <option value="Flexible">Flexible</option>
                                                            <option value="12 hours">12 hours</option>
                                                            <option value="18 hours">18 hours</option>
                                                            <option value="24 hours">24 hours</option>
                                                            <option value="36 hours">36 hours</option>
                                                            <option value="48 hours">48 hours</option>
                                                            <option value="72 hours">72 hours</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Quotation</label>
                                                <div class="col-md-5">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-dollar"></i>
                                                        </span>
                                                        <select name="quotation" id="quotation" class="form-control" data-error="Please select your quotation" required="">
                                                            <option value="" selected="selected">Quotation?</option>
                                                            <option value="Start work immediately">Start work immediately</option>
                                                            <option value="Start work and send quotation">Start work and send quotation</option>
                                                            <option value="Send quotation first">Send quotation first</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                    <label class="col-md-3 control-label">Instruction (Optional)</label>
                                                    <div class="col-md-7">
                                                        <textarea class="form-control" name="instruction" rows="3"></textarea>
                                                    </div>
                                                </div>





                                        </div>
                                        <div class="form-actions right">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-8">
                                                    <input type="hidden" name="reqtype" value="add">
                                                    <button type="submit" class="btn blue btn38">Submit & Upload Files</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>


                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->

            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <?php include('footer.php'); ?>
            <!-- END FOOTER -->
        </div>

        <!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<script src="assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/plugins/fuelux/js/spinner.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->

        <script src="assets/pages/scripts/components-bootstrap-touchspin.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>